# Wypoon React Coding Test

> A practical way to test React Coding skills

**Author:** Rogier van Poppel for [Wypoon Technologies](//www.wypoon.com).

In order for us to effectively validate coding skills we require candidates to take this test.
A completed version can be seen here (packed with create react app) : [react-test.wypoon.com](//react-test.wypoon.com).

The idea is to fork this repository in your own account and complete the given tasks. Push your code to your branch once you complete the test. On request, we will give you access to our code for the completed task.
Note that our methods are not neccassarily the best methods, so if you'd have any suggestions for improvements we will greatly welcome them.

***Please fork this repository in your own BitBucket account (register one if you do not have one) and set it to PRIVATE. This prevents other candidates from seeing your code***

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). If for whatever reason you do not have experience with that, please read their docs.

## Introduction

This Single page, basic react apps contains several components that displays data from the public Binance api websocket. 
There are some points that need improvement and better coding practice. Additionally, some other data needs to be displayed and some layout updates need to be included. 

### Included Packages
The packages included in this project *except for* the ones included by  [create-react-app](https://github.com/facebook/create-react-app).
*Stateless React Components for Bootstrap 4.
* [binance-api-node](https://github.com/binance-exchange/binance-api-node) : *A complete API wrapper for the Binance API.*
* [bootstrap](https://github.com/twbs/bootstrap) : *Sleek, intuitive, and powerful front-end framework for faster and easier web development.*
* [reactstrap](https://github.com/reactstrap/reactstrap) : *Stateless React Components for Bootstrap 4.*
* [bignumber.js](https://github.com/MikeMcl/bignumber.js) : *A JavaScript library for arbitrary-precision decimal and non-decimal arithmetic*

### The test
1. The wrapper component, **App**, contains a state-property named **pair**. This property is consumed by the other components. The **PairSelector** component shows a list of available pairs. Each pair already has a *onClick* callback set which gets the selected pair. Pass the selected pair to the state of the wrapper **App** component. 
2. Note that after updating the pair, the individual components still contain data and subscriptions to the old pair. Remove these upon updating the property. 
3. The **PairSelector** component contains a list of available pairs with volume, ticker (current price) and 24h change. THe list is limited to 20 entries but there are more entries available. Due to the nature of the updates, this list is constantly changing and reordering. Reorder the list by volume so that the 20  pairs with highest volume are shown and shown pairs will only occasionally change based on volume.
4. The pairs in the **PairSelector** component have a change property. Change the colour of the background of the rows to show positive or negative change (*hint:use bootstrap classes*)
5. The **RecentTrades** component contains a list of the most recent trades. Change the background colour of each trade to indicate sell or buy order (refer to *maker* property on the trade, and [binance-api-node](https://github.com/binance-exchange/binance-api-node) for more info). 
6. The **RecentTrades** limits its output to the last 20 trades. However, this is not shown correctly. Old trades are currently not removed from the list. Order the list so that the last trade is shown on top and, when the list reaches 21 entries, the oldest trade is removed. 
6. The **RecentTrades** component throws errors sometimes about duplicate keys due to the same trade coming in twice through the socket (see console). Prevent the list to contain duplicate keys.
8. The **OrderBook** component should show a list of current orders. Show the 10 lowest ask = sell orders and the 10 highest bid = buy orders. Update the list when new entries come in.
9. The **OrderBook** component shows a ticker with the current price of the selected pair. Highlight the background for .5s to indicate a negative or positive price change. Use transition. (see example [here](//react-test.wypoon.com).)
10. Each component has its own websocket subscriptions. This is very ineffecient. Find a solution to this so that only one websocket will be used.
11. The current layout is messy due to different lengths of data. Standardize the precision and/or length of given data.

*All the above steps are applied in the example on  [react-test.wypoon.com](//react-test.wypoon.com), if you are not sure about one of the assignments above, refer to the example.*

### When finished
When finished, push your code to your fork and inform your recruiter. He/She, or a technical representative will contact you to discuss your results.

***Make sure your repository is set to private, and allow access to your recruiter and user Rogier076 for evaluation***

Thank you for considering [Wypoon Technologies](//www.wypoon.com) and good luck with this assignment.

### Notes
This test might be extended in the near future with async tests and REST API
