import React, {Component} from 'react';
import {Navbar,Container,NavbarBrand} from 'reactstrap'
import logo from '../wypoon-logo.svg'

/**
 * Just the header
 *
 * @export
 * @class Header
 * @extends {Component}
 */
export default class Header extends Component {

    render(){
        return (
            <Navbar expand="md" className="d-flex flex-row justify-content-between px-0">
                <Container>
                    <NavbarBrand href="//www.wypoon.com">
                        <div className="d-flex flex-row">
                            <img className="img-fluid mr-2 mr-sm-3" src={logo} alt="wypoon  technologies" />
                            <div className="d-flex flex-column justify-content-center flex-wrap">
                                <h1 className="website-title m-0 d-flex flex-column flex-md-row">
                                    Wypoon&nbsp;<span>Technologies</span>
                                </h1>
                            </div>
                        </div>
                    </NavbarBrand>
                </Container>
            </Navbar>
        )
    }

}