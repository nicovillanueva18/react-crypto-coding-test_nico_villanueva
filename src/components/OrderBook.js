import React, {Component} from 'react';
import {Table,Card,CardHeader,CardTitle,CardBody} from 'reactstrap'
import Binance from 'binance-api-node';
import {BigNumber} from 'bignumber.js';
import Ticker from './Ticker';

export default class OrderBook extends Component {

    constructor(props){
        super(props);
        this.state = {
            message: "",
            ticker: new BigNumber(0)
        }
    }

    componentDidMount(){
        this.clean = Binance().ws.partialDepth({ symbol: this.props.pair, level: 20 }, depth => {
            //depth contains orderbook
        });
        this.clean2 = Binance().ws.ticker(this.props.pair, ticker => {
           this.setState(
               {
                   ticker: new BigNumber(ticker.bestAsk).plus(new BigNumber(ticker.bestBid)).multipliedBy(.5)
                }
            );
        });

    }

    componentWillUnmount(){
        typeof this.clean === "function" && this.clean();
        typeof this.clean2 === "function" && this.clean2();
    }


    render(){

        return(
            <Card>
                <CardHeader>
                    <CardTitle tag="h4" className="mb-0">
                        Orderbook
                    </CardTitle>
                </CardHeader>
                <CardBody className="p-0">
                    {this.state.message}
                <Table dark size="sm" className="mb-0">
                        <thead>
                            <tr>
                                <th>Price</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colSpan="2">
                                    Asks here
                                </td>
                            </tr>
                        </tbody>
                   </Table>
                   <Ticker className="d-none" price={this.state.ticker.toPrecision(10)}></Ticker>
                   <Table dark size="sm" className="mb-0">
                        <tbody>
                            <tr>
                                <td colSpan="2">
                                    Bids here
                                </td>
                            </tr>
                        </tbody>
                   </Table>
                </CardBody>
            </Card>
        );

    }
}