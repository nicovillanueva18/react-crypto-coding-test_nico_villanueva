import React,{Component} from 'react';

export default class Ticker extends  Component {

    render(){
        return(
            <h2 className="px-3 m-0 bg-dark text-white">{this.props.price}</h2>
        )
    }

}